import sqlite3

import jinja2
import shutil, os
from datetime import datetime
import csv

from openpyxl import Workbook


class ReportGenerator:
    def __init__(self, template_name='report-template.html'):
        self.templateLoader = jinja2.FileSystemLoader(searchpath="./templates/")
        self.templateEnv = jinja2.Environment(loader=self.templateLoader)
        self.TEMPLATE_FILE = template_name
        self.ASSETS_FOLDER = "templates/assets"

    # Util for directory copy
    # copytree2('templates/assets','test')
    @staticmethod
    def copytree2(source, dest):
        # os.mkdir(dest)
        dest_dir = os.path.join(dest, os.path.basename(source))
        shutil.copytree(source, dest_dir)

    # TODO: refactor total resulsts generation
    # Generate html report from template
    def report_html(self, report_path, results, tradetest, total_results):
        template = self.templateEnv.get_template(self.TEMPLATE_FILE)
        outputText = template.render(results=results, tradetest=tradetest, total_results=total_results)
        self.copytree2(self.ASSETS_FOLDER, report_path)
        with open(report_path + "report.html", "w") as fp:
            fp.write(outputText)
            fp.close()

    # Create report path based on time and date
    def get_report_path(self):
        timestmp = datetime.now()
        file_name = 'report.csv'
        report_dir_name = 'report_' + timestmp.strftime("%d-%m-%Y_%H-%M-%S")
        report_path = "reports/"
        path = report_path + report_dir_name
        os.makedirs(os.path.dirname(path + '/'), exist_ok=True)
        return path + '/'

    # CSV report to report path
    def report_csv(self, dir_path=None, results=None):
        filename = 'report.csv'

        with open(dir_path + filename, 'w', newline='') as myfile:
            wr = csv.writer(myfile, quoting=csv.QUOTE_ALL)

            for result in results:
                wr.writerow([result['parameters']['name'],
                             result['parameters']['symbol'],
                             result['parameters']['start_date'],
                             result['parameters']['end_date']])

            myfile.close()


def createxlsx_from_db(db_file_path='default.sqlite3', strategy='MACD', trend='bullish'):
    wb = Workbook()
    DB_connect = sqlite3.connect(db_file_path)
    DB_cursor = DB_connect.cursor()
    DB_cursor.execute('select * from backtest_table')
    results = DB_cursor.fetchall()

    ws = wb.active
    ws.title = strategy + ', ' + trend
    ws.append([
                  "symbol_name,strategy_plan,indicator,trend,start_date,end_date,stoploss_trigered,stoploss_level,takeprofit_level,entry_price"])
    for row in results:
        ws.append(row)
    workbook_name = strategy+ ', '+trend
    wb.save(workbook_name + ".xlsx")
    return  None
