import smtplib
import sqlite3
from datetime import datetime, timedelta


class Email:
    def __init__(self):
        self.sender = 'danieltwiki@gmail.com'
        self.sender_password = 'danielwiki'
        self.addressee = ['denzhvan@gmail.com', 'vkotsyubinsky@gmail.com']
        self.text = ''

    def connect(self):
        self.smtpObj = smtplib.SMTP('smtp.gmail.com', 587)
        self.encrypt()

    def encrypt(self):
        self.smtpObj.starttls()
        self.authorisation()

    def authorisation(self):
        self.smtpObj.login(self.sender, self.sender_password)

    def send_message(self):
        print('sending...')
        print(self.text)
        self.smtpObj.sendmail(self.sender, self.addressee, self.text)
        print('Sended succssesful y')


def send_to_addressee(message):
    mail = Email()
    mail.connect()
    mail.authorisation()
    mail.text = message
    mail.send_message()


def get_data_from_db(pattern_type='', get_all_signals=False):
    report_results = 'Subject:  {} STRATEGY\n'.format(pattern_type)
    db_file_path = 'default.sqlite3'
    todays_date = get_tommorrow_date(delta=1)
    DB_connect = sqlite3.connect(db_file_path)
    DB_cursor = DB_connect.cursor()

    if get_all_signals:
        DB_cursor.execute(
            """ SELECT * FROM founded_models WHERE trend='{}' order by stock; """.format(pattern_type))
    else:
        DB_cursor.execute(
            """ SELECT * FROM founded_models WHERE trend='{}'  and end_date='{}' order by stock; """.format(pattern_type,
                                                                                                         todays_date))
    result_string = ''
    for result in DB_cursor:
        result_string = "\r\n Symbol: " + str(result[1]) + " Started:" + str(
            result[4] + " Strategy:" + str(result[2]) + ' Indicator:' + str(result[6]) + " When added:" + str(
                result[7]) + "\n")
        report_results = report_results + result_string

    if result_string == '':
        report_results = report_results + "\r\n No signals "

    send_to_addressee(report_results)


def get_tommorrow_date(delta=0):
    today = datetime.now()
    tomorrow = today - timedelta(days=delta)
    return str(tomorrow.date())
