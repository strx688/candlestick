from datetime import datetime, timedelta


# TODO: Add feature for opening short postion
# Map backtesting results with results of tradetesting.

class Tradetester:

    def __init__(self, history_getter=None, profit_procents_target=3, stoploss=False, stoploss_level_procents=1):
        self.HG = history_getter
        self.stoploss = stoploss
        self.stoploss_level_procents = stoploss_level_procents
        self.profit_target = profit_procents_target

    def start(self, results, data_frame):
        for index, result in enumerate(results):
            error = False
            try:
                buy_index = result['parameters']['end_index'] - 1
                buy_date = data_frame['date'][buy_index]
                buy_price = data_frame['open'][buy_index]
                price_type = 'open'
            except KeyError:
                error = True

            trade_test = {
                "profit_index": 0,
                'graph_index': None,
                'profit_price': 0,
                'profit_date': 0,
                'profit': 'Error',
                'buy_price': 0,
                "stoploss_level": 0,
                "takeprofit_level": 0,

                'stoploss_trigered': 'Error'}
            results[index]['trade_test'] = trade_test

            for day_i in range(buy_index - 1, 0, -1):
                day = data_frame.loc[day_i]

                if error:
                    break

                today_price = day['open']

                if ((today_price / buy_price) * 100) >= (100 + self.profit_target):
                    profit_index = day['index']
                    profit_price = day['open']
                    profit_date = day['date']
                    profit = True
                    stoploss_trigered = False
                    trade_test = {
                        "profit_index": profit_index,
                        'graph_index': profit_index,
                        'profit_price': profit_price,
                        'profit_date': profit_date,
                        'profit': profit,
                        'buy_price': buy_price,
                        "stoploss_level": buy_price - (buy_price * (self.stoploss_level_procents / 100)),
                        "takeprofit_level": buy_price + (buy_price * (self.profit_target / 100)),

                        'stoploss_trigered': stoploss_trigered}

                    results[index]['trade_test'] = trade_test
                    break

                if self.stoploss == True:
                    if ((today_price / buy_price) * 100) <= (100 - self.stoploss_level_procents):
                        stoploss_trigered = True
                        profit = False
                        trade_test = {
                            "stoploss_index": day['index'],
                            'graph_index': day['index'],
                            "stoploss_price": day['open'],
                            "stoploss_date": day['date'],
                            'profit': profit,
                            'buy_price': buy_price,
                            "stoploss_level": buy_price - (buy_price * (self.stoploss_level_procents / 100)),
                            "takeprofit_level": buy_price + (buy_price * (self.profit_target / 100)),
                            'stoploss_trigered': stoploss_trigered}
                        results[index]['trade_test'] = trade_test

                        break
