import sqlite3

from strategy import Cup_left_side
from data_getter import DataGetter
from datetime import datetime
import seaborn as sns
import matplotlib.pyplot as plt
from mpl_finance import candlestick_ohlc
import uuid
import os
import matplotlib.dates as mdates
import plotly.graph_objects as go

import pandas as pd
from mpl_finance import candlestick_ohlc

import matplotlib

matplotlib.use('Agg')  # Bypass the need to install Tkinter GUI framework
import matplotlib.pyplot as plt
import matplotlib.dates as mdates

# Avoid FutureWarning: Pandas will require you to explicitly register matplotlib converters.
from pandas.plotting import register_matplotlib_converters

register_matplotlib_converters()


class Backtester:
    def __init__(self, report=False,
                 html_report=False,
                 trade_test=False,
                 data_getter=None,
                 preparator=None,
                 report_generator=None,
                 trade_tester=None):

        self.report = report
        self.html_report = html_report
        self.trade_test = trade_test
        self.DG = data_getter
        self.PREP = preparator
        self.RG = report_generator
        self.TT = trade_tester

    # Backtest  single symbol. Use slicing of dataframe
    def backtest(self, data_frame, strategy_plan=None, symbol=None, indicator_name='EMA'):
        results = []

        slicer = 0
        for x in range(data_frame.shape[0]):
            if len(data_frame[slicer:]) < 33: continue
            success, result = strategy_plan.start(data_frame[slicer:], symbol, original_df=data_frame,
                                                  indicator=indicator_name)
            if success == True:
                results.append(result)
                slicer = slicer + 1
            else:
                slicer = slicer + 1
        return results

    # TODO: make data structure for data preparing settings out of this method namespace
    # Backtest all symbols in DB
    def backtest_all_symbols(self, strategy_plan=None, symbols_limit=None, name_of_indicator='EMA'):
        all_indicators = {'EMA': self.PREP.calc_EMA, 'MACD': self.PREP.calc_MACD,
                          'CCI': self.PREP.calc_CCI, 'CMO': self.PREP.calc_CMO,
                          'RSI': self.PREP.calc_RSI, 'STOCH': self.PREP.calc_STOCH}
        DB_connect = sqlite3.connect('default.sqlite3')
        DB_cursor = DB_connect.cursor()
        DB_cursor.execute('delete from backtest_table')
        DB_connect.commit()

        if self.html_report or self.report:
            report_path = self.RG.get_report_path()

        all_res = []
        symbols = self.DG.get_symbols_list()

        if symbols_limit:
            symbols = symbols[:symbols_limit]
        i = -1  # counter of found models
        for symbol in symbols:
            print(symbol)
            data_frame = self.DG.get_symbols_data(symbol)
            if data_frame.empty: continue
            data_frame[name_of_indicator] = all_indicators[name_of_indicator](data_frame)
            data_frame = self.PREP.instant_trendline(data_frame)
            data_frame = self.PREP.df_index_revert(data_frame)
            results = self.backtest(data_frame=data_frame, strategy_plan=strategy_plan, symbol=symbol,
                                    indicator_name=name_of_indicator)

            start_indexes = []
            for res in results:
                s_x = res['parameters']['start_index']
                if s_x not in start_indexes:
                    start_indexes.append(s_x)

            saap = []
            sub_list = []
            for index in start_indexes:
                for res in results:
                    if index == res['parameters']['start_index']:
                        sub_list.append(res)
                saap.append(sub_list)
                sub_list = []

            new_res = []
            for sub in saap:
                range_list = []
                for each in sub:
                    val = each['parameters']['start_index'] - each['parameters']['end_index']
                    range_list.append(val)
                maxx = max(range_list)

                for evry in sub:
                    vall = evry['parameters']['start_index'] - evry['parameters']['end_index']
                    if vall == maxx:
                        new_res.append(evry)
                        break

            results = new_res

            if results != []:
                if self.trade_test:
                    self.TT.start(results, data_frame)

                if self.html_report:
                    for each in results:
                        # fig, ax = plt.subplots()
                        x = data_frame[each['parameters']['end_index']:each['parameters']['start_index'] + 1]
                        if self.trade_test and each['trade_test']['graph_index'] != None:
                            # x = data_frame[each['trade_test']['graph_index']:each['parameters']['start_index']+1]
                            x = data_frame[each['trade_test']['graph_index']:each['parameters']['start_index'] + 1]

                        loaded_data = x

                        # Convert 'Timestamp' to 'float'.
                        # candlestick_ohlc needs time to be in float days format - see date2num().
                        loaded_data['date'] = [mdates.date2num(d) for d in loaded_data['date']]

                        # Re-arrange data so that each row contains values of a day: 'date','open','high','low','close'.
                        quotes = [tuple(x) for x in loaded_data[['date', 'open', 'high', 'low', 'close']].values]

                        fig, ax = plt.subplots()
                        xtr, = ax.plot(loaded_data['date'], loaded_data[name_of_indicator], color='blue', label='EMA')
                        xtr.set_label('EMA')
                        ax.legend()

                        xtr, = ax.plot(loaded_data['date'], loaded_data['trend'], color='green', label='Trend')
                        xtr.set_label('Trend')
                        ax.legend()

                        for evry in each['subresults']:
                            s_d = evry['result']['end_date']
                            ax.axvline(x=s_d, linewidth=1)
                            ax.set_label(evry['parameters']['name'])
                            ax.legend()

                        if self.trade_test:
                            xtr = ax.axhline(y=each['trade_test']['stoploss_level'], color='r', linestyle='-')
                            xtr.set_label('stoploss')
                            ax.legend()

                            xtr = ax.axhline(y=each['trade_test']['takeprofit_level'], color='y', linestyle='-')
                            xtr.set_label('takeprofit')
                            ax.legend()

                        candlestick_ohlc(ax, quotes, width=0.5, colorup='g', colordown='r')

                        # Customize graph.
                        ##########################

                        plt.xlabel('Date')
                        plt.ylabel('Price')
                        plt.title(symbol)

                        # Format time.
                        ax.xaxis_date()
                        ax.xaxis.set_major_formatter(mdates.DateFormatter("%Y-%m-%d"))

                        plt.gcf().autofmt_xdate()  # Beautify the x-labels
                        plt.autoscale()

                        id = uuid.uuid4()
                        img_name = str(id) + '.png'

                        os.makedirs(report_path + 'imgs/', exist_ok=True)

                        plt.savefig(report_path + 'imgs/' + img_name)
                        each['img'] = 'imgs/' + img_name
                        each['uuid'] = str(id)
                        plt.clf()
                        i += 1

                all_res.extend(results)
                print(i)
                print(all_res)
                add_results_into_db(all_res[i])

        if self.report == True:
            self.RG.report_csv(dir_path=report_path, results=all_res)

        total_results = dict()
        total_results['symbols_count'] = len(symbols)
        total_results['strategy_name'] = strategy_plan.name

        if self.trade_test:
            profit_rate = 0
            total_pos = 0
            stoploss_rate = 0

            for result in all_res:
                total_pos = total_pos + 1
                try:
                    if result['trade_test']['profit'] is True:
                        profit_rate = profit_rate + 1
                    if result['trade_test']['stoploss_trigered'] is True:
                        stoploss_rate = stoploss_rate + 1

                    total_results['total_pos'] = total_pos

                    total_results['take_profit_target'] = self.TT.profit_target
                    total_results['stoploss_target'] = self.TT.stoploss_level_procents

                    total_results['profit_rate_perc'] = (profit_rate / total_pos) * 100
                    total_results['stoploss_rate_perc'] = (stoploss_rate / total_pos) * 100

                    total_results['profit_rate'] = profit_rate
                    total_results['stoploss_rate'] = stoploss_rate


                except KeyError:
                    print('exception triggered')
                    pass

        if self.html_report == True:
            self.RG.report_html(report_path, all_res, self.trade_test, total_results)

        return total_results


def add_results_into_db(all_res):
    symbol_name = all_res['parameters']['symbol']
    strategy_plan = all_res['parameters']['name']
    indicator = all_res['parameters']['name']
    trend = all_res['subresults'][0]['parameters']['trend']
    start_date = all_res['parameters']['start_date']
    end_date = all_res['parameters']['end_date']
    stoploss_trigered = all_res['trade_test']['stoploss_trigered']
    stoploss_level = all_res['trade_test']['stoploss_level']
    takeprofit_level = all_res['trade_test']['takeprofit_level']
    buy_price = all_res['trade_test']['buy_price']
    DB_connect = sqlite3.connect('default.sqlite3')
    DB_cursor = DB_connect.cursor()
    DB_cursor.execute("INSERT INTO backtest_table(symbol_name,strategy_plan,indicator,trend,start_date,"
                      "end_date,stoploss_trigered,stoploss_level,takeprofit_level,entry_price) "
                      "VALUES('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s')"
                      % (symbol_name, strategy_plan, indicator, trend, start_date,
                         end_date, stoploss_trigered, stoploss_level, takeprofit_level, buy_price))
    DB_connect.commit()
    return None
