import sqlite3

import numpy as np
import math
import talib
from datetime import datetime


class StrategyPlan:
    def __init__(self, name=None, strategies=[], backtest=False):
        self.backtest = backtest
        self.strategies = strategies
        self.name = name
        self.results = []

    def make_result(self, symbol=None, pattern_type=None, indicator='EMA', entry_price=0):
        dates = []
        index = []
        for result in self.results:
            dates.append(result['result']['start_date'])
            dates.append(result['result']['end_date'])
            index.append(result['result']['end_index'])
            index.append(result['result']['start_index'])

        cleared_dates = []

        # for date in dates:
        #     cleared_dates.append(date.astype(datetime.datetime))

        end_date = max(dates)
        start_date = min(dates)

        end_index = min(index)
        start_index = max(index)

        result = {'parameters': {'name': self.name,
                                 'symbol': symbol,
                                 'start_date': start_date,
                                 'end_date': end_date,
                                 'end_index': end_index,
                                 'start_index': start_index},
                  'subresults': self.results
                  }
        if not self.backtest:
            todays_date = str(datetime.now().date())
            add_if_pattern_not_exsists_in_db(symbol, self.name, pattern_type, start_date, end_date, todays_date,
                                             indicator, entry_price)
        return result

    def start(self, data_frame, symbol=None, original_df=None, pattern_type='bearish', indicator='EMA'):
        self.results = []
        start_point = 0
        for strategy in self.strategies:
            success, start_point, result, entry_price = strategy.start(data_frame=data_frame,
                                                                       start_index=start_point,
                                                                       results=self.results,
                                                                       original_df=original_df, symbol=symbol)
            self.results.append(result)

            start_point = start_point
            if success == False:
                self.results = []
                return False, False
        return True, self.make_result(symbol=symbol, pattern_type=pattern_type, indicator=indicator,
                                      entry_price=entry_price)


class Cup_left_side:
    def __init__(self, width=10, step_delimiter=5, precision=80, start_index=0, height_delta=5):
        self.width = width
        self.step_delimiter = step_delimiter
        self.precision = precision
        self.start_index = start_index
        self.name = 'Cup left side'
        self.height_delta = height_delta

    def start(self, data_frame=None, start_index=None, **kwargs):
        if start_index != None:
            self.start_index = start_index

        if data_frame is None:
            return False, False, False

        if self.start_index == 0:
            sliced_df = data_frame[self.start_index:(self.start_index + self.width)]
        else:
            sliced_df = data_frame.loc[self.start_index + 1:(self.start_index + +self.width)]

        while True:
            if self.width % self.step_delimiter == 0:
                self.step_size = self.width / self.step_delimiter
                try:
                    df_list = [sliced_df.loc[idx] for idx in np.split(sliced_df.index, self.step_delimiter)]
                except ValueError:
                    return False, False, False
                break
            else:
                self.step_delimiter = self.step_delimiter + 1

        tgs = []
        for df in df_list:
            tg = df['ema'][-1:].values[0] / self.step_size
            tgs.append(tg)

        results = []

        for i in range(len(tgs)):
            if i + 1 < len(tgs):
                if tgs[i] <= tgs[i + 1]:
                    results.append(True)
                else:
                    results.append(False)
            else:
                break

        one_proc = 100 / len(results)
        precision = one_proc * results.count(True)

        if precision >= self.precision:
            success = True
        else:
            success = False

        s_p_low = sliced_df.head(1)['low'].values[0]
        s_p_high = sliced_df.head(1)['high'].values[0]

        con_low = kwargs['results'][0]['result']['min_price_level']
        con_high = kwargs['results'][0]['result']['max_price_level']

        if s_p_low < con_low or s_p_high > con_high:
            success = False

        high = max(sliced_df['high'])
        low = min(sliced_df['low'])
        delta = 100 - ((low / high) * 100)

        if delta < self.height_delta:
            success = False

        end_index = sliced_df.iloc[0]['index']
        start_index = sliced_df.iloc[-1]['index']

        print("Precision:", precision, '%')
        print("Result:", success)
        print("Start index:", start_index)
        print("End index:", end_index)
        print("Start date:", data_frame['date'][start_index])
        print("End date:", data_frame['date'][end_index])

        result = {'parameters': {'name': self.name,
                                 'width': self.width,
                                 'step_delimiter': self.step_delimiter,
                                 'step_size': self.step_size,
                                 'target_precision': self.precision
                                 },
                  'result': {'precision': precision,
                             'result': success,
                             'start_index': start_index,
                             'end_index': end_index,
                             'start_date': data_frame['date'][start_index],
                             'end_date': data_frame['date'][end_index]

                             }
                  }

        return success, start_index, result


class Hammer:
    def __init__(self, pattern_type='bullish', delta=5, break_low_consil_level=False, **kwargs):
        self.break_low_consil_level = break_low_consil_level
        self.delta = delta / 100
        self.name = 'Hammer'
        self.pattern_type = pattern_type

    def start(self, data_frame=None, start_index=0, **kwargs):
        df = data_frame
        hammer = False
        hammer_index = 0
        hammer_date = df.iloc[0]['date']
        low = 0
        if start_index == 0 or start_index is None:
            if df.iloc[0]['open'] > ((df.iloc[0]['high'] - df.iloc[0]['low']) * 75 / 100) + df.iloc[0]['low'] and \
                    df.iloc[0]['close'] > ((df.iloc[0]['high'] - df.iloc[0]['low']) * 75 / 100) + df.iloc[0]['low']:
                low = df.iloc[0]['low']
                point_value = calc_value_point_for_hammer(df, 0, self.delta, self.pattern_type)
                print(kwargs['symbol'])
                if self.break_low_consil_level:
                    point_value = False
                if point_value or self.break_low_consil_level:
                    hammer_trend = df.iloc[0]['trend_b']
                    hammer = True
                    hammer_index = 0
                    hammer_date = df.iloc[0]['date']
        else:
            if df.loc[start_index]['open'] > (
                    (df.loc[start_index]['high'] - df.loc[start_index]['low']) * 75 / 100) \
                    + df.loc[start_index]['low'] \
                    and df.loc[start_index]['close'] > \
                    ((df.loc[start_index]['high'] - df.loc[start_index]['low']) * 75 / 100) + df.loc[start_index][
                'low']:

                point_value = calc_value_point_for_hammer(df, start_index, self.delta, self.pattern_type)

                if self.break_low_consil_level:
                    point_value = False
                if point_value or self.break_low_consil_level:
                    hammer_trend = df.loc[start_index]['trend_b']
                    hammer = True
                    hammer_index = start_index
                    hammer_date = df.loc[start_index]['date']
        if hammer:
            success = True
            if self.pattern_type:
                success = False
                if self.pattern_type == 'bearish' and hammer_trend == True:
                    success = True
                if self.pattern_type == 'bullish' and hammer_trend == False:
                    success = True
        else:
            success = False

        result = {'parameters': {'name': self.name,
                                 'trend': self.pattern_type},

                  'result': {'result': success,
                             'start_index': hammer_index,
                             'end_index': hammer_index,
                             'start_date': hammer_date,
                             'end_date': hammer_date,
                             'min_price': low
                             }}
        if success:
            print('This is result of Hammer')
            print(result)
        return success, hammer_index + 1, result


class Engulfing:
    def __init__(self, pattern_type='bullish', delta=5):
        self.delta = delta / 100
        self.name = 'Engulfing'
        self.pattern_type = pattern_type

    def calc_candle_type(self, candle):
        open_p = candle['candle_df']['open']
        close_p = candle['candle_df']['close']

        if open_p > close_p:
            candle['type'] = 'red'

        if close_p > open_p:
            candle['type'] = 'green'

        if close_p == open_p:
            candle['type'] = 'grey'

    def calc_pattern_type(self, candle_one, candle_two):
        if candle_one['type'] == 'red' and candle_two['type'] == 'green':
            return 'bullish'
        if candle_one['type'] == 'green' and candle_two['type'] == 'red':
            return 'bearish'
        return None

    def calc_pattern(self, pattern_type, candle_one, candle_two, df, start_index):
        max_point_value = calc_point_value_df(df, self.delta, 'max', start_index)
        min_point_value = calc_point_value_df(df, self.delta, 'min', start_index)
        if pattern_type == 'bearish':
            if candle_one['candle_df']['open'] > candle_two['candle_df']['close'] and candle_one['candle_df'][
                'close'] <= candle_two['candle_df']['open'] and candle_one['candle_df']['close'] >= max_point_value:
                return True

        if pattern_type == 'bullish':
            if candle_one['candle_df']['close'] >= candle_two['candle_df']['open'] and candle_one['candle_df']['open'] < \
                    candle_two['candle_df']['close'] and candle_one['candle_df']['close'] <= min_point_value:
                return True

        return False

    def start(self, data_frame=None, start_index=None, **kwargs):
        df = data_frame
        success = False
        end_index = 0
        start_date = 0
        end_date = 0
        fisrt_candle = {}
        second_candle = {}

        row, col = df.shape
        if row > 2:
            if start_index == 0 or start_index is None:
                fisrt_candle['candle_df'] = df.iloc[1]
                second_candle['candle_df'] = df.iloc[0]

                start_index = df.iloc[1]['index']
                end_index = df.iloc[0]['index']

                start_date = df.iloc[1]['date']
                end_date = df.iloc[0]['date']

            else:

                fisrt_candle['candle_df'] = df.loc[start_index + 1]
                second_candle['candle_df'] = df.loc[start_index]

                start_index = df.loc[start_index + 1]
                end_index = df.loc[start_index]

                start_date = df.loc[start_index + 1]
                end_date = df.loc[start_index]

            self.calc_candle_type(fisrt_candle)
            self.calc_candle_type(second_candle)

            eng_type = self.calc_pattern_type(fisrt_candle, second_candle)

            if eng_type:
                if eng_type == self.pattern_type:
                    success = self.calc_pattern(self.pattern_type, fisrt_candle, second_candle, df, start_index)

        result = {'parameters': {'name': self.name,
                                 'trend': self.pattern_type},

                  'result': {'result': success,
                             'start_index': start_index,
                             'end_index': end_index,
                             'start_date': start_date,
                             'end_date': end_date
                             }}
        if success:
            print(result)
        return success, start_index + 1, result


class Consolidation2:
    def __init__(self, height_delta=3, width=10, hammer_break_consil=False):
        self.hammer_break_consil = hammer_break_consil
        self.height_delta = height_delta
        self.width = width
        self.name = 'Consolidation'

    def start(self, data_frame=None, **kwargs):
        if data_frame is None:
            return False, False, False

        if data_frame.empty:
            return False, False, False

        delta = 0
        low = 0
        high = 0
        success = False
        last_index = kwargs['original_df'].tail(1)['index'].values[0]
        final_index = last_index - self.width

        df = data_frame[0:self.width]

        last_slice_index = df.tail(1)['index'].values[0]

        if last_slice_index >= final_index:
            success = False
        else:
            high = max(df['high'])
            low = min(df['low'])
            delta = 100 - ((low / high) * 100)
            if self.hammer_break_consil:
                low_hammer_price = kwargs['results'][0]['result']['min_price']
                success_hammer = kwargs['results'][0]['result']['result']
                if delta > self.height_delta:
                    print('Consolidation not found!!!')

                elif delta < self.height_delta and low_hammer_price <= low and success_hammer:
                    print('Consolidation found')
                    print(low_hammer_price)
                    print(kwargs['symbol'])
                    success = True
            else:
                if delta > self.height_delta:
                    print('Consolidation not found!!!')
                    success = False
                else:
                    print('Consolidation found')
                    success = True

        self.consolidation_start_index = df.tail(1)['index'].values[0]
        self.consolidation_end_index = df.head(1)['index'].values[0]

        self.consolidation_start_date = df.tail(1)['date'].values[0]
        self.consolidation_end_date = df.head(1)['date'].values[0]

        result = {'parameters': {'name': self.name,
                                 'width': self.width,
                                 'height_delta': self.height_delta,
                                 'trend': 'Consolidation'
                                 },
                  'result': {
                      'min_price_level': low,
                      'max_price_level': high,
                      'consolidation_length': self.width,
                      'consolidation_delta_calculated:': delta,
                      'result': success,
                      'start_index': self.consolidation_start_index,
                      'end_index': self.consolidation_end_index,
                      'start_date': self.consolidation_start_date,
                      'end_date': self.consolidation_end_date}}
        x = self.consolidation_start_index
        return success, x, result

    #
    # if candle_two['candle_df']['close'] > candle_two['candle_df']['open'] \
    #           and candle_one['candle_df']['open'] > candle_one['candle_df']['close'] \
    #           and candle_one['candle_df']['open'] >= candle_two['candle_df']['close'] \
    #           and candle_two['candle_df']['open'] >= candle_one['candle_df']['close'] \
    #           and ((candle_one['candle_df']['open']-candle_one['candle_df']['close'])>(candle_two['candle_df']['close']-candle_two['candle_df']['open'])):


class Double_Divergency:

    # write exception about null result
    def __init__(self, days_delta=11, pattern_type='bearish', indicator_name='EMA', diff_btw_max=1):
        self.indicator_name = indicator_name
        self.days_delta = days_delta
        self.name = 'Double_Divergency'
        self.pattern_type = pattern_type
        self.diff_btw_max = diff_btw_max / 100

    def start(self, data_frame=None, start_index=None, **kwargs):
        success = False

        start_index = data_frame.index[0]
        end_index = 0
        start_date = 0
        end_date = 0
        if self.pattern_type == 'bullish':
            # extremums are min values
            first_range = self.days_delta
            second_range = self.days_delta * 3

            first_stock_data_frame_range = data_frame['low'][:first_range]
            second_stock_data_frame_range = data_frame['low'][first_range:second_range]

            first_ind_data_frame_range = data_frame[self.indicator_name][:first_range]
            second_ind_data_frame_range = data_frame[self.indicator_name][first_range:second_range]

            first_extremum_stock = data_frame['low'][start_index]
            first_range_max_stock = first_stock_data_frame_range.min()

            if first_extremum_stock == first_range_max_stock:

                first_extremum_indicator = data_frame[self.indicator_name][start_index]
                first_range_max_indicator = first_ind_data_frame_range.min()

                if first_extremum_indicator == first_range_max_indicator:
                    second_extremum_stock = second_stock_data_frame_range.min()
                    second_extremum_indicator = second_ind_data_frame_range.min()
                    end_index = second_stock_data_frame_range.idxmin()
                    max_ind_frame = second_ind_data_frame_range.idxmin()
                    if end_index == max_ind_frame:

                        if check_its_maximum(kwargs['original_df'], first_extremum_stock, first_extremum_indicator,
                                             second_extremum_stock, second_extremum_indicator, self.days_delta,
                                             start_index, self.indicator_name, self.pattern_type):

                            if first_extremum_stock < second_extremum_stock and first_extremum_indicator > second_extremum_indicator:

                                diff = (
                                               second_extremum_stock - first_extremum_stock) >= second_extremum_stock * self.diff_btw_max
                                # diff_ind = (first_extremum_indicator-second_extremum_indicator)>=first_extremum_indicator*self.diff_btw_max
                                if diff:
                                    success = True
                                    start_date = data_frame['date'][start_index]
                                    end_date = data_frame['date'][end_index]

        elif self.pattern_type == 'bearish':
            # extremums are max values
            first_range = self.days_delta
            second_range = self.days_delta * 3

            first_stock_data_frame_range = data_frame['high'][:first_range]
            second_stock_data_frame_range = data_frame['high'][first_range:second_range]

            first_ind_data_frame_range = data_frame[self.indicator_name][:first_range]
            second_ind_data_frame_range = data_frame[self.indicator_name][first_range:second_range]

            first_extremum_stock = data_frame['high'][start_index]
            first_range_max_stock = first_stock_data_frame_range.max()

            if first_extremum_stock == first_range_max_stock:

                first_extremum_indicator = data_frame[self.indicator_name][start_index]
                first_range_max_indicator = first_ind_data_frame_range.max()

                if first_extremum_indicator == first_range_max_indicator:
                    second_extremum_stock = second_stock_data_frame_range.max()
                    second_extremum_indicator = second_ind_data_frame_range.max()
                    end_index = second_stock_data_frame_range.idxmax()
                    max_ind_frame = second_ind_data_frame_range.idxmax()
                    if end_index == max_ind_frame:

                        if check_its_maximum(kwargs['original_df'], first_extremum_stock, first_extremum_indicator,
                                             second_extremum_stock, second_extremum_indicator, self.days_delta,
                                             start_index, self.indicator_name, self.pattern_type):

                            if first_extremum_stock > second_extremum_stock and first_extremum_indicator < second_extremum_indicator:
                                diff = (
                                               first_extremum_stock - second_extremum_stock) >= first_extremum_stock * self.diff_btw_max
                                # diff_ind = (second_extremum_indicator-first_extremum_indicator)>=second_extremum_indicator*self.diff_btw_max
                                if diff:
                                    success = True
                                    start_date = data_frame['date'][start_index]
                                    end_date = data_frame['date'][end_index]

        result = {'parameters': {'name': self.name,
                                 'trend': self.pattern_type},

                  'result': {'result': success,
                             'start_index': start_index,
                             'end_index': end_index,
                             'start_date': start_date,
                             'end_date': end_date
                             }}
        return success, start_index + 1, result, first_extremum_stock


class Consolidation:
    def __init__(self, indicator, ma_period=50, range=10):
        self.indicator = indicator
        self.range = range
        self.ma_period = ma_period
        self.name = 'Consoliadtion'
        self.pattern_type = 'consolidation'

    def start(self, data_frame=None, start_index=None, **kwargs):
        success = False
        start_index = data_frame.index[0]
        end_index = start_index+self.range
        start_date = 0
        end_date = 0
        if self.search_consolidation(data_frame['EMA']):
            success = True
            start_date = data_frame['date'][start_index]
            end_date = data_frame['date'][end_index]

        result = {'parameters': {'name': self.name,
                                 'trend': self.pattern_type},

                  'result': {'result': success,
                             'start_index': start_index,
                             'end_index': end_index,
                             'start_date': start_date,
                             'end_date': end_date
                             }}
        return success, start_index + 1, result, 0

    def search_consolidation(self, indicator_data):
        max = indicator_data[0] + (indicator_data[0] * 0.01)
        min = indicator_data[0] - (indicator_data[0] * 0.01)
        counter = 0
        for i, data in enumerate(indicator_data):
            if i == 0: continue
            if data > max or data < min:
                counter += 1
        if counter >= self.range * 0.2:
            return False
        return True


# функція для перевірки максимумів: перевіряє чи дані значення є  максимумами зліва і справа
# (відносно графіка) на delta*5 індексів
def check_its_maximum(data_frame, maximum_stock, maximum_indicator, second_maximum_stock, second_maximum_indicator,
                      delta, start_index, indicator, pattern_type):
    left_range = start_index + delta * 3 + delta
    right_range = start_index - delta
    main_range = start_index + delta * 3

    if start_index < delta:
        if pattern_type == 'bearish':
            if (second_maximum_stock > data_frame['high'][main_range:left_range].max() and
                    second_maximum_indicator > data_frame[indicator][main_range:left_range].max()):
                return True
        elif pattern_type == 'bullish':
            if (second_maximum_stock < data_frame['low'][main_range:left_range].min() and
                    second_maximum_indicator < data_frame[indicator][main_range:left_range].min()):
                return True

    if pattern_type == 'bearish':
        if (maximum_stock > data_frame['high'][right_range:start_index].max() and
                maximum_indicator > data_frame[indicator][right_range:start_index].max() and
                second_maximum_stock > data_frame['high'][main_range:left_range].max() and
                second_maximum_indicator > data_frame[indicator][main_range:left_range].max()):
            return True
    elif pattern_type == 'bullish':
        if (maximum_stock < data_frame['low'][right_range:start_index].min() and
                maximum_indicator < data_frame[indicator][right_range:start_index].min() and
                second_maximum_stock < data_frame['low'][main_range:left_range].min() and
                second_maximum_indicator < data_frame[indicator][main_range:left_range].min()):
            return True
    return None


def add_if_pattern_not_exsists_in_db(symbol, strategy, trend_pattern, start_date, end_date, todays_date, indicator,
                                     entry_price):
    position = ''
    if trend_pattern == 'bullish':
        position = 'long'
    elif trend_pattern == 'bearish':
        position = 'short'

    db_file_path = 'default.sqlite3'
    DB_connect = sqlite3.connect(db_file_path)
    DB_cursor = DB_connect.cursor()
    DB_cursor.execute(
        "SELECT stock,strategy_plan,start_date FROM founded_models WHERE stock='%s' AND strategy_plan='%s' AND "
        "start_date='%s' "
        "and updated = '%s' AND trend='%s' AND indicator='%s' and end_date='%s'" % (
            symbol, strategy, start_date, todays_date, trend_pattern, indicator, end_date))
    if not DB_cursor.fetchall():
        DB_cursor.execute(
            "INSERT INTO founded_models(stock,strategy_plan,trend,start_date,end_date,indicator,updated,position,status,entry_price)"
            "VALUES ('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s')"
            % (symbol, strategy, trend_pattern, start_date,
               end_date, indicator, todays_date, position, 'waiting', entry_price))
    DB_connect.commit()


# calculate point that is max or min border for buying/selling
def calc_point_value_df(df, delta, extreme='', start_index=0):
    if df is None: return False
    if df.empty: return False
    if start_index == 0:
        df = df
    else:
        df = df[start_index:start_index + 20]
    if extreme == 'min':
        min_value_in_df = df['close'].min()
        return min_value_in_df * delta + min_value_in_df
    elif extreme == 'max':
        max_value_in_df = df['close'].max()
        return max_value_in_df - max_value_in_df * delta
    return False


def calc_value_point_for_hammer(df, start_index, delta, pattern_type):
    if df is None: return False
    if df.empty: return False
    m = start_index + 20
    df = df[:m]
    try:
        if pattern_type == 'bearish':
            max_point_value = calc_point_value_df(df, delta, 'max')
            if df.iloc[start_index]['close'] >= max_point_value:
                return True

        if pattern_type == 'bullish':
            min_point_value = calc_point_value_df(df, delta, 'min')
            print(min_point_value)
            if df.iloc[start_index]['close'] <= min_point_value:
                return True
    except:
        return False
    return False
