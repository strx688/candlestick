import pandas as pd
import talib


class DataPreparator:
    def __init__(self, data_frame=None):
        self.data_frame = data_frame

    def calc_EMA(self, data_frame=None, period=50):
        if data_frame is None:
            data_frame = self.data_frame
        EMA = talib.EMA(data_frame['close'], timeperiod=period,)
        return EMA

    def calc_MACD(self, data_frame=None, fastperiod=12, slowperiod=26, signalperiod=9):
        if data_frame is None:
            data_frame = self.data_frame

        MACD = talib.MACD(data_frame['close'], fastperiod, slowperiod, signalperiod,)
        return MACD[0][1:]

    def calc_CCI(self, data_frame=None, period=14):
        if data_frame is None:
            data_frame = self.data_frame

        CCI = talib.CCI(data_frame['high'], data_frame['low'], data_frame['close'], timeperiod=period,)
        return CCI[1:]

    def calc_CMO(self, data_frame=None, period=14):
        if data_frame is None:
            data_frame = self.data_frame
        CMO = talib.CMO(data_frame['close'],timeperiod=period, )
        return CMO[ 1:]

    def calc_RSI(self, data_frame=None, period=14):
        if data_frame is None:
            data_frame = self.data_frame

        RSI = talib.RSI(data_frame['close'], timeperiod=period)
        return RSI[1:]

    def calc_STOCH(self, data_frame=None, fastk_period=5, slowk_period=3, slowk_matype=0, slowd_period=3, slowd_matype=0):
        if data_frame is None:
            data_frame = self.data_frame

        _, STOCH = talib.STOCH(data_frame['high'], data_frame['low'], data_frame['close'],
                               fastk_period=fastk_period, slowk_period=slowk_period,
                               slowk_matype=slowk_matype, slowd_period=slowd_period, slowd_matype=slowd_matype)
        return STOCH[1:]

    def df_index_revert(self, df):
        data_frame = df.iloc[::-1].reset_index(drop=True)
        data_frame['index'] = data_frame.index
        return data_frame

    # Use Hilbert transform for determinating trend.( Have unstable period)
    # Map boolean trend var for each bar
    def instant_trendline(self, df):
        df['trend'] = talib.HT_TRENDLINE(df['close'])
        df = df.dropna(axis=0, how='any')
        df['trend_b'] = ''
        for index, each in enumerate(df['trend']):
            try:
                if each > df['trend'].iloc()[int(index) + 1]:
                    df['trend_b'].iloc()[index] = False
                else:
                    df['trend_b'].iloc()[index] = True
            except IndexError:
                if index > df['trend'].iloc()[int(index) - 1]:
                    df['trend_b'].iloc()[index] = True

        return df

    def trend_by_ema(self, df):
        df = df.dropna(axis=0, how='any')
        df['trend_b'] = ''
        for index, each in enumerate(df['EMA']):
            try:
                if each > df['EMA'].iloc()[int(index) + 1]:
                    df['trend_b'].iloc()[index] = False
                else:
                    df['trend_b'].iloc()[index] = True
            except IndexError:
                if index > df['EMA'].iloc()[int(index) - 1]:
                    df['trend_b'].iloc()[index] = True

        return df
