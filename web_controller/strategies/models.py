from django.db import models


hammer_trend_choices = (('none','none'),
                        ('up','up'),
                        ('down','down'))


class Hammer(models.Model):
    on_trend = models.CharField(max_length=30, choices=hammer_trend_choices, default=hammer_trend_choices[0][0])
