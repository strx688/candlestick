from django.db import models
from django.contrib.auth.models import AbstractUser


def user_avatar_upload_path(instance, filename):
    return 'users/{0}/avatar/{1}'.format(instance.username, filename)

class User(AbstractUser):
    email = models.EmailField(unique=True)

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['username',]

    def __str__(self):
        return '{0} {1}'.format(self.first_name, self.last_name)