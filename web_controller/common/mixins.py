class ActiveTabMixin(object):
    active_tab = 'default'

    def get_context_data(self, **kwargs):
        context = super(ActiveTabMixin, self).get_context_data(**kwargs)
        context['active_tab'] = self.active_tab

        return context
