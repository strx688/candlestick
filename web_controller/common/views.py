from django.shortcuts import render
from django.views.generic import TemplateView
from .mixins import ActiveTabMixin

class OverviewView(ActiveTabMixin,TemplateView):
    active_tab = 'overview'
    template_name = "overview.html"

