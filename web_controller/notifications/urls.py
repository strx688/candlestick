from django.urls import path,re_path
from .views import NotificationsSettingsView,RecipientUpdate, RecipientCreate, RecipientDelete
urlpatterns = [


    path('', NotificationsSettingsView.as_view(), name="notif_settings__page"),
    path('add/', RecipientCreate.as_view(), name='author-add'),
    path('<int:pk>/update/', RecipientUpdate.as_view(), name='author-update'),
    path('<int:pk>/delete/', RecipientDelete.as_view(), name='author-delete'),

]
