from django.db import models

class Recipient(models.Model):
    email_switch = models.BooleanField(default=True)
    email = models.EmailField(max_length=300)

