from django.shortcuts import render
from django.urls import reverse_lazy
from django.views.generic.edit import CreateView, DeleteView, UpdateView
from .models import Recipient
from django.views.generic import TemplateView
from common.mixins import ActiveTabMixin

class NotificationsSettingsView(ActiveTabMixin,TemplateView):
    active_tab = 'notifications-settings'
    template_name = "notifications/notifications-settings.html"



class RecipientCreate(CreateView):
    model = Recipient
    fields = ['email']

class RecipientUpdate(UpdateView):
    model = Recipient
    fields = ['email']

class RecipientDelete(DeleteView):
    model = Recipient
    success_url = reverse_lazy('author-list')