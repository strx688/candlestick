import os
from time import sleep
import pandas as pd
from ib_insync import *
import sqlite3
from ib_insync import util


class DataGetter:

    # Update symbols: update symbols list in DB from txt file
    def __init__(self, db_file_path='default.sqlite3', update_symbols=False):
        self.DB_connect = sqlite3.connect(db_file_path)
        self.DB_cursor = self.DB_connect.cursor()
        self.db_file_path = db_file_path

        symbols_table_exist = self.DB_cursor.execute(""" SELECT * FROM sqlite_master WHERE name ='symbols' and type='table'; """).fetchall()

        if symbols_table_exist == []:
            self.DB_cursor.execute(""" CREATE TABLE symbols (name text, status BOOLEAN) """)
            self.DB_connect.commit()
            update_symbols =True

        if update_symbols == True:
            self.update_symbols()



    def update_symbols(self,symbols_file_path='symbols.txt'):
        with open(symbols_file_path,'r') as symbols_file:
            symbols_list = symbols_file.read().splitlines()
            symbols_file.close()
        symbols_list = [(str(symbol), 1) for symbol in symbols_list]

        self.DB_cursor.execute("""DELETE FROM symbols;""")
        self.DB_cursor.executemany(" INSERT INTO symbols VALUES (?,?)",symbols_list)

        self.DB_connect.commit()


    # Update historical data in DB for each symbol stored in DB
    def getHistoricalData(self,symbol="", duration='4 Y'):
        ib = IB()

        connect_flag = True
        while connect_flag:
            try:
                ib.connect('77.121.97.137', 4001, 1)
                connect_flag = False
            except:
                sleep(10)
                connect_flag = True
        connect_flag = True


        active_symbols = self.DB_connect.execute(""" SELECT name FROM symbols where status=1""").fetchall()
        active_symbols = [each[0] for each in active_symbols]
        gathered_symbols = []
        for symbol in active_symbols:
            contract = Stock(symbol, 'SMART', 'USD', primaryExchange='NASDAQ')
            bars = ib.reqHistoricalData(contract, endDateTime='', durationStr=duration,
                                    barSizeSetting='1 day', whatToShow='TRADES', useRTH=True)
            df = util.df(bars)
            if df is not None:
                print('DONE:'+str(symbol))
                df.to_sql(str(symbol), con=self.DB_connect, if_exists='replace')
                gathered_symbols.append(symbol)
            else:
                self.DB_connect.execute('UPDATE symbols SET status = "0" WHERE name =?',(symbol,))
                self.DB_connect.commit()
        ib.disconnect()
        return gathered_symbols

    # Return dataframe of historical data by symbol name
    def get_symbols_data(self,symbol):
        self.DB_connect_path = "sqlite:///" + self.db_file_path
        df = pd.read_sql_table(str(symbol), self.DB_connect_path)
        return df


    # Get list of symbols present in DB
    def get_symbols_list(self):
            active_symbols = self.DB_connect.execute(""" SELECT name FROM symbols where status=1""").fetchall()
            active_symbols = [each[0] for each in active_symbols]
            return active_symbols



    def get_symbol_new_data(self,symbol,duration='30 D'):
        ib = IB()

        connect_flag = True
        while connect_flag:
            try:
                ib.connect('77.121.97.137', 4001, 1)
                connect_flag = False
                contract = Stock(symbol, 'SMART', 'USD', primaryExchange='NASDAQ')
                bars = ib.reqHistoricalData(contract, endDateTime='', durationStr=duration,
                                barSizeSetting='1 day', whatToShow='TRADES', useRTH=True)
                df = util.df(bars)
                ib.disconnect()
                if df is not None:
                    return df
                else:
                    return None
            except:
                sleep(10)
                print('No connection. Try again in 0.1s... ')
                connect_flag = True
        return None





def get_symbol_data(symbol,duration='30 D'):
        ib = IB()

        connect_flag = True
        while connect_flag:
            try:
                ib.connect('77.121.97.137', 4001, 1)
                connect_flag = False
                contract = Stock(symbol, 'SMART', 'USD', primaryExchange='NASDAQ')
                bars = ib.reqHistoricalData(contract, endDateTime='', durationStr=duration,
                                            barSizeSetting='1 day', whatToShow='TRADES', useRTH=True)
                df = util.df(bars)
                ib.disconnect()
                if df is not None:
                    return df
                else:
                    return None
            except:
                sleep(10)
                print('No connection. Try again in 0.1s... ')
                connect_flag = True
        return None


