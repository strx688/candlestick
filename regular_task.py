import copy
import sqlite3

from data_getter import DataGetter
from data_preparator import DataPreparator
from strategy import Cup_left_side, StrategyPlan, Hammer, Consolidation2, Engulfing, Double_Divergency, Consolidation
from email_class import Email
import pandas as pd
from email_class import get_data_from_db
from datetime import datetime
from data_getter import get_symbol_data
from collections import Counter
import os


def check_all_position(stop_loss=2, take_profit=2, trend_pattern='bearish', db_file_path='default.sqlite3',
                       status='waiting', save_to_excel=False):
    todays_date = datetime.now().date()
    DB_connect = sqlite3.connect(db_file_path)

    query = f"select * from founded_models where status ='{status}' and trend='{trend_pattern}' "
    df = pd.read_sql(query, DB_connect)
    if save_to_excel:
        path = f'portfolio/{todays_date}'
        try:
            os.mkdir(path)
        except FileExistsError:
            print(f'{path} is exists')
        df = pd.read_sql("select * from founded_models", DB_connect)
        status_var = Counter(df['status'])
        for variant in status_var.keys():
            tmp_df = get_data_by_param(dataframe=df, column='status', param=variant)
            tmp_df.to_excel(f"{path}/{variant}.{todays_date}.xlsx")
        new_df = get_confirmed_stocks(df)
        new_df.to_excel(f"{path}/RSI+MACD confirmed.{todays_date}.xlsx")

        return None
    for i, stock in enumerate(df['stock']):
        print(stock)
        check_symbol(i, df, stop_loss, take_profit, trend_pattern)


def get_data_by_param(dataframe, column='', param=''):
    return dataframe[dataframe[column].isin([param])]


def get_confirmed_stocks(df):
    unique_dates = Counter(df['end_date'])
    sort_dict = {}
    for date in unique_dates:
        tmp = []
        for i, stock in enumerate(df['stock']):
            if df['end_date'][i] == date: tmp.append(stock)
        sort_dict.update({date: tmp})

    list_df = []
    for date in unique_dates:
        tmp_df = get_data_by_param(dataframe=df, column='end_date', param=date)
        value_counts = pd.DataFrame(tmp_df['stock'].value_counts())
        all_stocks = value_counts.index
        confirmed_stocks = []
        confirmed_stocks.append(
            [all_stocks[i] for i, repeat_value in enumerate(value_counts['stock']) if repeat_value == 2])

        print(confirmed_stocks)

        for confirmed_stock in confirmed_stocks[0]:
            try:
                list_df.append(get_data_by_param(dataframe=tmp_df, column='stock', param=confirmed_stock))
            except IndexError:
                print(f'not confirmed')
        print(list_df)
    new_df = pd.concat(list_df)
    return new_df


def check_symbol(i, df, stop_loss, take_profit, trend_pattern):
    try:
        new_price = get_symbol_data(df['stock'][i], '1 D')['close'][0]
    except  TypeError:
        return None
    stop_loss_price = df['entry_price'][i] - df['entry_price'][i] * (stop_loss / 100)
    take_profit_price = df['entry_price'][i] + df['entry_price'][i] * (take_profit / 100)

    if trend_pattern == 'bullish':
        old_price = df['entry_price'][i]
        if new_price > take_profit_price:
            calc_profit(new_price, old_price, df['stock'][i], df['indicator'][i], trend_pattern, )
        if new_price > old_price:
            update_position(df['stock'][i], df['indicator'][i], trend_pattern, 'open')
        if new_price <= stop_loss_price:
            calc_profit(new_price, old_price, df['stock'][i], df['indicator'][i], trend_pattern, )

    elif trend_pattern == 'bearish':
        old_price = df['entry_price'][i]
        if new_price < stop_loss_price:
            calc_profit(new_price, old_price, df['stock'][i], df['indicator'][i], trend_pattern, )
        if new_price < old_price:
            update_position(df['stock'][i], df['indicator'][i], trend_pattern, 'open')
        if new_price >= take_profit_price:
            calc_profit(new_price, old_price, df['stock'][i], df['indicator'][i], trend_pattern, )
    return None


def calc_profit(new_price, old_price, symbol, indicator, trend_pattern, ):
    profit = new_price - old_price
    if trend_pattern == 'bearish': profit = - profit
    update_position(symbol, indicator, trend_pattern, 'close', new_price, profit)
    return None


def update_position(symbol, indicator, trend_pattern, status, exit_price=0, profit=0):
    todays_date = datetime.now().date()
    db_file_path = 'default.sqlite3'
    DB_connect = sqlite3.connect(db_file_path)
    DB_cursor = DB_connect.cursor()
    DB_cursor.execute("UPDATE founded_models SET status = '%s',exit_price='%s',profit = '%s',updated='%s' "
                      "WHERE stock='%s' and indicator='%s' and trend = '%s'"
                      % (status, exit_price, profit, todays_date, symbol, indicator, trend_pattern))
    DB_connect.commit()
    return None


def calc(strategy, indicators, indicators_name, dur, pattern_type='bullish', double_divergence=False, ):
    GETTER = DataGetter(update_symbols=False)
    symbols_list = GETTER.get_symbols_list()
    for item in strategy.keys():
        strategy_plan = StrategyPlan(name=str(item))
        strategy_plan.strategies = strategy[item]
        print('Started!')
        for index, symbol in enumerate(symbols_list):
            df = GETTER.get_symbol_new_data(symbol, duration=dur)
            if df is None: continue
            if not df.empty:
                df[indicators_name] = indicators[indicators_name](data_frame=df)
                df = PREPARATOR.df_index_revert(df)
                strategy_plan.start(data_frame=df, symbol=symbol, original_df=df, pattern_type=pattern_type,
                                    indicator=indicators_name)
                print('Done:' + str(round((index / len(symbols_list)) * 100)) + '%')


PREPARATOR = DataPreparator()
all_indicators = {'EMA': PREPARATOR.calc_EMA, 'MACD': PREPARATOR.calc_MACD,
                  'CCI': PREPARATOR.calc_CCI, 'CMO': PREPARATOR.calc_CMO,
                  'RSI': PREPARATOR.calc_RSI, 'STOCH': PREPARATOR.calc_STOCH}

# all_pattern_types = ['bullish', 'bearish']
# for pattern in all_pattern_types:
#     pattern_type = pattern
#     indicator_name = 'EMA'
#     eng = Engulfing(pattern_type=pattern_type)
#     ham = Hammer(pattern_type=pattern_type)
#     consill = Consolidation2(height_delta=5, width=10)
#     duration = '40 D'
#     strategies = {'Hammer+engulfing': [copy.deepcopy(eng), copy.deepcopy(ham)],
#                   'Hammer': [copy.deepcopy(ham)],
#                   'Engulfing': [copy.deepcopy(eng)],
#                   'Consolidation+hammer': [copy.deepcopy(ham), copy.deepcopy(consill)],
#                   }
#
#     calc(strategies, all_indicators, indicator_name, duration, pattern_type=pattern_type)
#
# all_pattern_types = ['consolidation']
# for pattern in all_pattern_types:
#     pattern_type = pattern
#     indicator_name = 'EMA'
#     consill = Consolidation2(height_delta=5, width=10)
#
#     duration = '40 D'
#     strategies = {'Consolidation': [copy.deepcopy(consill)], }
#
#     calc(strategies, all_indicators, indicator_name, duration, pattern_type=pattern_type)
#
# all_pattern_types = ['bullish', 'bearish']
# for pattern in all_pattern_types:
#     pattern_type = pattern
#     indicator_name = ['MACD', 'RSI']
#     duration = '100 D'
#
#     for ind in indicator_name:
#         dd = Double_Divergency(indicator_name=ind, pattern_type=pattern_type, diff_btw_max=3)
#         strategies = {'Double Divergence': [copy.deepcopy(dd)], }
#         calc(strategies, all_indicators, ind, duration, pattern_type=pattern_type, double_divergence=True)
#
# for pattern in ['bullish', 'bearish']:
#     get_data_from_db(pattern, get_all_signals=False)


# check_all_position(trend_pattern='bullish', status='open', save_to_excel=False)


indicator_name = ['EMA']
duration = '100 D'

for ind in indicator_name:
    cc = Consolidation(indicator=ind)
    strategies = {'Consolidation': [copy.deepcopy(cc)], }
    calc(strategies, all_indicators, ind, duration, double_divergence=False)

for pattern in ['bullish', 'bearish']:
    get_data_from_db(pattern, get_all_signals=False)
