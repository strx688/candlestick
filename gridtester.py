import numpy as np
import csv




class Gridtester:
    def __init__(self,back_tester,tradetester):
        self.Backtester = back_tester
        self.Backtester.report = False
        self.Backtester.html_report = False

        self.Tradetester = tradetester
        self.Tradetester.stoploss = True

        self.Backtester.TT = self.Tradetester

        self.Backtester.trade_test = True



    def set_profit_grid(self,step,min,max):
        self.profit_step = step
        self.profit_min = min
        self.profit_max = max

    def set_stoploss_grid(self, step, min, max):
        self.stoploss_step = step
        self.stoploss_min = min
        self.stoploss_max = max


    def run_grid(self,strategy_plan,symbol_limit=None):

        results = []

        for stoploss_target in np.arange(self.stoploss_min, self.stoploss_max, self.stoploss_step):

            for profit_target in np.arange(self.profit_min, self.profit_max, self.profit_step):
                self.Tradetester.stoploss_level_procents = stoploss_target
                self.Tradetester.profit_target = profit_target
                result = self.Backtester.backtest_all_symbols(strategy_plan, symbol_limit)
                results.append(result)

        filename = 'report.csv'
        with open(filename, 'w', newline='') as myfile:
            wr = csv.writer(myfile, quoting=csv.QUOTE_ALL)
            for result in results:
                wr.writerow([
                             'Symbols_count: '+ str(result['symbols_count']),
                             'Stoploss target: '+ str(result['stoploss_target']),
                             'Takeprofit target: ' + str(result['take_profit_target']),
                             'Total BUY positions: '+str(result['total_pos']),
                             'Profit rate%: '+str(result['profit_rate_perc']),
                             'Stoploss rate%: '+ str(result['stoploss_rate_perc'])

                ])

            myfile.close()