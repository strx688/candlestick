from data_getter import DataGetter
from data_preparator import DataPreparator
from strategy import Cup_left_side, StrategyPlan, Hammer, Consolidation2, Engulfing
from backtester import Backtester
from report_generator import ReportGenerator, createxlsx_from_db
from tradetester import Tradetester
from gridtester import Gridtester
from strategy import Double_Divergency




GETTER = DataGetter(update_symbols=False)
# GETTER.getHistoricalData()

PREPARATOR = DataPreparator()

RG = ReportGenerator()

TT = Tradetester(history_getter=GETTER,
                 stoploss=True,
                 profit_procents_target=2,
                 stoploss_level_procents=2)

pattern_type = 'bearish'
indicator_name = 'MACD'

ham = Hammer(pattern_type=pattern_type)
eng = Engulfing(pattern_type=pattern_type)
consil = Consolidation2(height_delta=5, width=10, hammer_break_consil=True)

dd = Double_Divergency(pattern_type=pattern_type, indicator_name=indicator_name,diff_btw_max=3)
strategy_plan = StrategyPlan(name=indicator_name, backtest=True)

strategy_plan.strategies.append(dd)

BACKTESTER = Backtester(report=True,
                        html_report=True,
                        trade_test=True,
                        data_getter=GETTER,
                        preparator=PREPARATOR,
                        report_generator=RG,
                        trade_tester=TT)

BACKTESTER.backtest_all_symbols(strategy_plan, name_of_indicator=indicator_name)


createxlsx_from_db()
#
# GRIDTESTER = Gridtester(back_tester=BACKTESTER,tradetester=TT)
# GRIDTESTER.set_profit_grid(0.5, 0.5, 20.5)
# GRIDTESTER.set_stoploss_grid(0.5, 0.5, 20.5)
# GRIDTESTER.run_grid(strategy_plan)
